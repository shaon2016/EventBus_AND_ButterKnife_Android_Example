package shaoniiuc.com.eventbusfragmentexample.event;

/**
 * Created by Shaon on 11/16/2016.
 */

public class AnotherMessageEvent {
    private String message;

    public AnotherMessageEvent(String message) {
        this.message = message;
    }

    public String  getMessage() {
        return message;
    }
}
