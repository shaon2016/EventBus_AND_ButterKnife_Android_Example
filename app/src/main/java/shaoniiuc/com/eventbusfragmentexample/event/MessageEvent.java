package shaoniiuc.com.eventbusfragmentexample.event;

/**
 * Created by Shaon on 11/16/2016.
 */

public class MessageEvent {
    private String message;

    public MessageEvent(String message) {
        this.message = message;
    }

    public String  getMessage() {
        return message;
    }
}
