package shaoniiuc.com.eventbusfragmentexample.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import shaoniiuc.com.eventbusfragmentexample.fragment.FirstFragment;
import shaoniiuc.com.eventbusfragmentexample.fragment.SecondFragment;


/**
 * Created by Shaon on 11/16/2016.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public static final int FIRST = 0;
    public static final int SECOND = 1;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case FIRST:
                fragment = FirstFragment.newInstance("", "");
                break;
            case SECOND:
                fragment = SecondFragment.newInstance("", "");
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case FIRST: return "First";
            case SECOND: return "Second";
        }
        return null;
    }
}
