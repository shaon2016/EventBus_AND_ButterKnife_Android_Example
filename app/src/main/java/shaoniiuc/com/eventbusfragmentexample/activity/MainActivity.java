package shaoniiuc.com.eventbusfragmentexample.activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import shaoniiuc.com.eventbusfragmentexample.R;
import shaoniiuc.com.eventbusfragmentexample.adapter.ViewPagerAdapter;
import shaoniiuc.com.eventbusfragmentexample.event.MessageEvent;
import shaoniiuc.com.eventbusfragmentexample.fragment.SecondFragment;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.editText) EditText editText;
    @BindView(R.id.textMainActivity) TextView text;
    @BindView(R.id.btnShowText) Button btnShowText;
    @BindView(R.id.btnSecondActivity) Button btnSecondActivity;

    EventBus eventBus = EventBus.getDefault();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        btnSecondActivity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(MainActivity.this, FragmentActivity.class));
//            }
//        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent messageEvent) {
        this.text.setText(messageEvent.getMessage());
    }


    @OnClick(R.id.btnSecondActivity)
    public void goToSecondActivity(Button button) {
        startActivity(new Intent(MainActivity.this, FragmentActivity.class));
    }

    @OnClick(R.id.btnShowText)
    public void postStickyEvent(Button button) {
        eventBus.postSticky(new MessageEvent(editText.getText().toString()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        eventBus.unregister(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }
}
