Copyright 2016, Md Ashiqul Islam, All rights reserved, You may take help or modify my code. Don't copy paste whole code for your purpose

In this example i implement butterknife and eventbus library.


Butter Knife is small, simple and lightweight, and it makes life as a developer easier.
It allows developers to perform injection on arbitrary objects, views and OnClickListeners so they can focus on writing useful code. 
Consider Android Butter Knife a reduction library. It replaces findViewById with @Bind() and set^^^^Listener calls with @onClick() making code cleaner and more understandable.
Butter Knife enables focus on logic instead of glue code and reduces development time by reducing redundant coding.


EventBus is an Android optimized publish/subscribe event bus.
A typical use case for Android apps is gluing Activities, Fragments, and background threads together.
Conventional wiring of those elements often introduces complex and error-prone dependencies and life cycle issues.